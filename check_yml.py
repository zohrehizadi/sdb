from __future__ import print_function
from glob import glob
import os
import sys
import yaml
import copy
from optparse import OptionParser

from sdb.experiment import Experiment
from sdb.utilities import lc_dict
from sdb.xps import state_Jolly1984

parser = OptionParser()
parser.add_option("-v", "--verbose",
                  action="store_true", dest="verbose", default=False,
                  help="verbose")
(options, args) = parser.parse_args()

nicename = {
    'coreelectronbindingenergy' : 'CoreElectronBindingEnergy',
    'reference' : 'Reference',
    'unit' : 'Unit',
    'index' : 'Atom',
    'atom' : 'Atom',
}

def collect(dirname, fname='experimental.yml'):
    fc = []
    for root, dirs, files in os.walk(dirname):
        for fn in files:
            if fn == fname:
                fc.append(os.path.join(root, fn))
        for direc in dirs:
            fc += collect(os.path.join(root, direc))
    return fc

if len(args) > 0:
    filelist = args
else:
    filelist = collect('molecules')
    filelist = collect('solids')

if options.verbose:
    p = print
else:
    def p(*args):
        return
    
for fn in filelist:
    print('checking', fn)
    changed = False

    alldata = yaml.load(open(fn, 'r'))
    lc = lc_dict(alldata)
    try:
        xpsdata = lc['coreelectronbindingenergy']
        p('xpsdata=', xpsdata)
        if type(xpsdata) != type([]):
            xpsdata = [xpsdata]

        for entry in xpsdata:
            p('  entry=', entry)
            # each entry may define several atom types
            print('sj', state_Jolly1984)
            for element in state_Jolly1984:
                p('element', element, state_Jolly1984[element])
                vstate = '{0}({1})'.format(element, state_Jolly1984[element])
                p('vstate', vstate)
                if vstate in entry:
                    edata = entry[vstate]
                    # atom indicees might be specified
                    p(' ', element, edata)
                    if type(edata) == type([]):
                        # multiple entries
                        for eentry in edata:
                            if 'atoms' in eentry:
                                # lazy notation -> transform
                                changed = True
                                eentry['Atom'] = [
                                    int(i) for i in 
                                    eentry.pop('atoms').split(',')]
                            if 'index' in eentry:
                                changed = True
                                eentry['Atom'] = eentry.pop('index')
                    else:
                        ## print(' ', element, edata)
                        pass
    except KeyError:
        pass

                    ##print(' ', element, edata)
        if changed:
#            print(yaml.dump(alldata, default_flow_style=False))
            with open(fn, 'w') as yaml_file:
                yaml_file.write(yaml.dump(alldata, default_flow_style=False))
            print('  corrected')


    except ValueError:
        print('  no XPS data.')
