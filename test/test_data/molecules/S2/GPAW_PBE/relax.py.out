
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.1.1b1
 |___|_|             

User:   mw@coco
Date:   Fri Mar 19 08:19:34 2021
Arch:   x86_64
Pid:    20716
Python: 3.8.5
gpaw:   /home/mw/source/gpaw/origin/gpaw (2043601477)
_gpaw:  /home/mw/source/gpaw/origin/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (7af1277ae5)
ase:    /home/mw/source/ase/origin/ase (version 3.22.0b1-19426a49ad)
numpy:  /home/mw/.local/lib/python3.8/site-packages/numpy (version 1.19.2)
scipy:  /home/mw/.local/lib/python3.8/site-packages/scipy (version 1.5.3)
libxc:  4.3.4
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  h: 0.2
  nbands: -10
  occupations: {fixmagmom: False,
                name: fermi-dirac,
                width: 0.1}
  spinpol: False
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

S-setup:
  name: Sulfur
  id: 3047e8a236aa99a9e65ffc390ede0609
  Z: 16.0
  valence: 6
  core: 10
  charge: 0.0
  file: /home/mw/source/gpaw-setups/gpaw-setups-0.6.6300/S.PBE.gz
  compensation charges: gauss, rc=0.27, lmax=2
  cutoffs: 1.49(filt), 1.66(core),
  valence states:
                energy  radius
    3s(2.00)   -17.254   0.847
    3p(4.00)    -7.008   0.847
    *s           9.957   0.847
    *p          20.203   0.847
    *d           0.000   0.847

  Using partial waves for S as LCAO basis

Reference energy: -21715.239799

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 0.0001 electrons
  Maximum integral of absolute eigenstate change: 4e-08 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 16

  ( 1  0  0)  ( 1  0  0)  ( 1  0  0)  ( 1  0  0)  ( 1  0  0)  ( 1  0  0)
  ( 0  1  0)  ( 0  1  0)  ( 0  0  1)  ( 0  0  1)  ( 0  0 -1)  ( 0  0 -1)
  ( 0  0  1)  ( 0  0 -1)  ( 0  1  0)  ( 0 -1  0)  ( 0  1  0)  ( 0 -1  0)

  ( 1  0  0)  ( 1  0  0)  (-1  0  0)  (-1  0  0)  (-1  0  0)  (-1  0  0)
  ( 0 -1  0)  ( 0 -1  0)  ( 0  1  0)  ( 0  1  0)  ( 0  0  1)  ( 0  0  1)
  ( 0  0  1)  ( 0  0 -1)  ( 0  0  1)  ( 0  0 -1)  ( 0  1  0)  ( 0 -1  0)

  (-1  0  0)  (-1  0  0)  (-1  0  0)  (-1  0  0)
  ( 0  0 -1)  ( 0  0 -1)  ( 0 -1  0)  ( 0 -1  0)
  ( 0  1  0)  ( 0 -1  0)  ( 0  0  1)  ( 0  0 -1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: Uniform real-space grid
  Kinetic energy operator: 6*3+1=19 point O(h^6) finite-difference Laplacian
  ScaLapack parameters: grid=1x1, blocksize=None
  Wavefunction extrapolation:
    Improved wavefunction reuse through dual PAW basis 

  Fermi-Dirac: width=0.1000 eV

Eigensolver
   Davidson(niter=2, smin=None, normalize=True) 

Densities:
  Coarse grid: 52*40*40 grid
  Fine grid: 104*80*80 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 104*80*80 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [1, 2, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 86.95 MiB
  Calculator: 6.57 MiB
    Density: 2.68 MiB
      Arrays: 1.91 MiB
      Localized functions: 0.35 MiB
      Mixer: 0.41 MiB
    Hamiltonian: 1.28 MiB
      Arrays: 1.25 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.03 MiB
    Wavefunctions: 2.61 MiB
      Arrays psit_nG: 1.10 MiB
      Eigensolver: 1.47 MiB
      Projections: 0.00 MiB
      Projectors: 0.04 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 8
Number of bands in calculation: 16
Number of valence electrons: 12
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
Creating initial wave functions:
  8 bands from LCAO basis set
  8 bands from random numbers

      .-------------------------.  
     /|                         |  
    / |                         |  
   /  |                         |  
  /   |                         |  
 *    |                         |  
 |    |                         |  
 |    |       S    S            |  
 |    |                         |  
 |    |                         |  
 |    .-------------------------.  
 |   /                         /   
 |  /                         /    
 | /                         /     
 |/                         /      
 *-------------------------*       

Positions:
   0 S      4.245218    4.000000    4.000000    ( 0.0000,  0.0000,  0.0000)
   1 S      6.154782    4.000000    4.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    10.400000    0.000000    0.000000    52     0.2000
  2. axis:    no     0.000000    8.000000    0.000000    40     0.2000
  3. axis:    no     0.000000    0.000000    8.000000    40     0.2000

  Lengths:  10.400000   8.000000   8.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.2000

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  08:19:35                  -6.464569    1      
iter:   2  08:19:35  -0.24  -1.29    -6.491629    1      
iter:   3  08:19:35  -0.55  -1.44    -6.490231    1      
iter:   4  08:19:36  -1.28  -1.87    -6.487953    1      
iter:   5  08:19:36  -1.61  -2.21    -6.487595    1      
iter:   6  08:19:36  -2.14  -2.96    -6.487843    1      
iter:   7  08:19:37  -2.57  -3.13    -6.487928    1      
iter:   8  08:19:37  -2.97  -3.52    -6.487971    1      
iter:   9  08:19:37  -3.36  -3.72    -6.487986    1      
iter:  10  08:19:37  -3.74  -3.92    -6.487993    1      
iter:  11  08:19:38  -4.12  -4.10    -6.487996    1      
iter:  12  08:19:38  -4.49  -4.31    -6.487998    1      
iter:  13  08:19:38  -4.86  -4.51    -6.487998    1      
iter:  14  08:19:38  -5.23  -4.90    -6.487998    1      
iter:  15  08:19:39  -5.59  -4.94    -6.487998    1      
iter:  16  08:19:39  -5.96  -5.24    -6.487998    1      
iter:  17  08:19:39  -6.31  -5.51    -6.487999    1      
iter:  18  08:19:39  -6.67  -5.69    -6.487999    1      
iter:  19  08:19:40  -7.03  -5.91    -6.487999    1      
iter:  20  08:19:40  -7.38  -6.14    -6.487999    1      
iter:  21  08:19:40  -7.74  -6.37    -6.487999    1      

Converged after 21 iterations.

Dipole moment: (0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -21715.239799)

Kinetic:         +7.064529
Potential:       -7.851629
External:        +0.000000
XC:              -5.662318
Entropy (-ST):   -0.277259
Local:           +0.100049
--------------------------
Free energy:     -6.626628
Extrapolated:    -6.487999

 Band  Eigenvalues  Occupancy
    0    -20.71271    2.00000
    1    -15.27030    2.00000
    2     -9.72641    2.00000
    3     -8.78363    2.00000
    4     -8.78363    2.00000
    5     -5.29231    1.00000
    6     -5.29231    1.00000
    7     -1.20346    0.00000
    8      0.39992    0.00000
    9      1.91626    0.00000
   10      1.91691    0.00000
   11      1.96810    0.00000
   12      2.01973    0.00000
   13      2.02850    0.00000
   14      2.86150    0.00000
   15      3.13328    0.00000

Fermi level: -5.29231

Gap: 0.000 eV
Transition (v -> c):
  (s=0, k=0, n=5, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=6, [0.00, 0.00, 0.00])

Forces in eV/Ang:
  0 S    -0.14471    0.00000    0.00000
  1 S     0.14471    0.00000    0.00000

      Step     Time          Energy         fmax
*Force-consistent energies used in optimization.
FIRE:    0 08:19:40       -6.626628*       0.1447
System changes: positions 

Initializing position-dependent things.

Density initialized from wave functions
      .-------------------------.  
     /|                         |  
    / |                         |  
   /  |                         |  
  /   |                         |  
 *    |                         |  
 |    |                         |  
 |    |       S    S            |  
 |    |                         |  
 |    |                         |  
 |    .-------------------------.  
 |   /                         /   
 |  /                         /    
 | /                         /     
 |/                         /      
 *-------------------------*       

Positions:
   0 S      4.243771    4.000000    4.000000    ( 0.0000,  0.0000,  0.0000)
   1 S      6.156229    4.000000    4.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    10.400000    0.000000    0.000000    52     0.2000
  2. axis:    no     0.000000    8.000000    0.000000    40     0.2000
  3. axis:    no     0.000000    0.000000    8.000000    40     0.2000

  Lengths:  10.400000   8.000000   8.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.2000

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  08:19:40  -3.73           -6.488311    1      
iter:   2  08:19:41  -4.29  -3.18    -6.488297    1      
iter:   3  08:19:41  -4.12  -3.40    -6.488292    1      
iter:   4  08:19:41  -5.24  -4.17    -6.488292    1      
iter:   5  08:19:41  -5.43  -4.09    -6.488292    1      
iter:   6  08:19:41  -5.83  -4.49    -6.488292    1      
iter:   7  08:19:42  -6.77  -4.70    -6.488292    1      
iter:   8  08:19:42  -7.35  -4.94    -6.488292    1      
iter:   9  08:19:42  -7.59  -5.21    -6.488292    1      

Converged after 9 iterations.

Dipole moment: (-0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -21715.239799)

Kinetic:         +6.864647
Potential:       -7.691999
External:        +0.000000
XC:              -5.622665
Entropy (-ST):   -0.277259
Local:           +0.100354
--------------------------
Free energy:     -6.626922
Extrapolated:    -6.488292

 Band  Eigenvalues  Occupancy
    0    -20.69365    2.00000
    1    -15.27899    2.00000
    2     -9.72453    2.00000
    3     -8.77432    2.00000
    4     -8.77432    2.00000
    5     -5.29989    1.00000
    6     -5.29989    1.00000
    7     -1.22335    0.00000
    8      0.40065    0.00000
    9      1.91767    0.00000
   10      1.91770    0.00000
   11      1.96942    0.00000
   12      2.00399    0.00000
   13      2.02399    0.00000
   14      2.63675    0.00000
   15      3.13140    0.00000

Fermi level: -5.29989

Gap: 3.474 eV
Transition (v -> c):
  (s=0, k=0, n=4, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=5, [0.00, 0.00, 0.00])

Forces in eV/Ang:
  0 S    -0.05786    0.00000    0.00000
  1 S     0.05786    0.00000    0.00000

FIRE:    1 08:19:42       -6.626922*       0.0579
System changes: positions 

Initializing position-dependent things.

Density initialized from wave functions
      .-------------------------.  
     /|                         |  
    / |                         |  
   /  |                         |  
  /   |                         |  
 *    |                         |  
 |    |                         |  
 |    |       S    S            |  
 |    |                         |  
 |    |                         |  
 |    .-------------------------.  
 |   /                         /   
 |  /                         /    
 | /                         /     
 |/                         /      
 *-------------------------*       

Positions:
   0 S      4.241745    4.000000    4.000000    ( 0.0000,  0.0000,  0.0000)
   1 S      6.158255    4.000000    4.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    10.400000    0.000000    0.000000    52     0.2000
  2. axis:    no     0.000000    8.000000    0.000000    40     0.2000
  3. axis:    no     0.000000    0.000000    8.000000    40     0.2000

  Lengths:  10.400000   8.000000   8.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.2000

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  08:19:42  -3.44           -6.488327    1      
iter:   2  08:19:42  -4.02  -3.03    -6.488299    1      
iter:   3  08:19:43  -3.86  -3.26    -6.488290    1      
iter:   4  08:19:43  -4.99  -4.01    -6.488290    1      
iter:   5  08:19:43  -5.21  -3.94    -6.488290    1      
iter:   6  08:19:43  -5.53  -4.34    -6.488290    1      
iter:   7  08:19:43  -6.39  -4.59    -6.488290    1      
iter:   8  08:19:44  -7.02  -4.72    -6.488290    1      
iter:   9  08:19:44  -7.27  -4.99    -6.488290    1      
iter:  10  08:19:44  -8.05  -5.16    -6.488290    1      

Converged after 10 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -21715.239799)

Kinetic:         +6.591383
Potential:       -7.474089
External:        +0.000000
XC:              -5.567791
Entropy (-ST):   -0.277259
Local:           +0.100838
--------------------------
Free energy:     -6.626919
Extrapolated:    -6.488290

 Band  Eigenvalues  Occupancy
    0    -20.66713    2.00000
    1    -15.29107    2.00000
    2     -9.72181    2.00000
    3     -8.76136    2.00000
    4     -8.76136    2.00000
    5     -5.31041    1.00000
    6     -5.31041    1.00000
    7     -1.25118    0.00000
    8      0.40180    0.00000
    9      1.92126    0.00000
   10      1.92127    0.00000
   11      1.97164    0.00000
   12      2.00353    0.00000
   13      2.01825    0.00000
   14      2.43461    0.00000
   15      3.12964    0.00000

Fermi level: -5.31041

Gap: 4.059 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])

Forces in eV/Ang:
  0 S     0.05958    0.00000    0.00000
  1 S    -0.05958    0.00000    0.00000

FIRE:    2 08:19:44       -6.626919*       0.0596
System changes: positions 

Initializing position-dependent things.

Density initialized from wave functions
      .-------------------------.  
     /|                         |  
    / |                         |  
   /  |                         |  
  /   |                         |  
 *    |                         |  
 |    |                         |  
 |    |       S    S            |  
 |    |                         |  
 |    |                         |  
 |    .-------------------------.  
 |   /                         /   
 |  /                         /    
 | /                         /     
 |/                         /      
 *-------------------------*       

Positions:
   0 S      4.241894    4.000000    4.000000    ( 0.0000,  0.0000,  0.0000)
   1 S      6.158106    4.000000    4.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    10.400000    0.000000    0.000000    52     0.2000
  2. axis:    no     0.000000    8.000000    0.000000    40     0.2000
  3. axis:    no     0.000000    0.000000    8.000000    40     0.2000

  Lengths:  10.400000   8.000000   8.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.2000

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  08:19:44  -5.71           -6.488307    1      
iter:   2  08:19:44  -6.29  -4.17    -6.488306    1      
iter:   3  08:19:45  -6.15  -4.40    -6.488306    1      
iter:   4  08:19:45  -7.25  -5.16    -6.488306    1      
iter:   5  08:19:45  -7.47  -5.07    -6.488306    1      

Converged after 5 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -21715.239799)

Kinetic:         +6.612077
Potential:       -7.490890
External:        +0.000000
XC:              -5.571865
Entropy (-ST):   -0.277259
Local:           +0.101001
--------------------------
Free energy:     -6.626936
Extrapolated:    -6.488306

 Band  Eigenvalues  Occupancy
    0    -20.66906    2.00000
    1    -15.29015    2.00000
    2     -9.72199    2.00000
    3     -8.76229    2.00000
    4     -8.76229    2.00000
    5     -5.30962    1.00000
    6     -5.30962    1.00000
    7     -1.24912    0.00000
    8      0.40172    0.00000
    9      1.92098    0.00000
   10      1.92099    0.00000
   11      1.97140    0.00000
   12      2.00304    0.00000
   13      2.01862    0.00000
   14      2.37877    0.00000
   15      3.12967    0.00000

Fermi level: -5.30962

Gap: 0.000 eV
Transition (v -> c):
  (s=0, k=0, n=5, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=6, [0.00, 0.00, 0.00])

Forces in eV/Ang:
  0 S     0.05072    0.00000    0.00000
  1 S    -0.05072    0.00000    0.00000

FIRE:    3 08:19:45       -6.626936*       0.0507
System changes: positions 

Initializing position-dependent things.

Density initialized from wave functions
      .-------------------------.  
     /|                         |  
    / |                         |  
   /  |                         |  
  /   |                         |  
 *    |                         |  
 |    |                         |  
 |    |       S    S            |  
 |    |                         |  
 |    |                         |  
 |    .-------------------------.  
 |   /                         /   
 |  /                         /    
 | /                         /     
 |/                         /      
 *-------------------------*       

Positions:
   0 S      4.242170    4.000000    4.000000    ( 0.0000,  0.0000,  0.0000)
   1 S      6.157830    4.000000    4.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    10.400000    0.000000    0.000000    52     0.2000
  2. axis:    no     0.000000    8.000000    0.000000    40     0.2000
  3. axis:    no     0.000000    0.000000    8.000000    40     0.2000

  Lengths:  10.400000   8.000000   8.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.2000

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  08:19:45  -5.17           -6.488331    1      
iter:   2  08:19:45  -5.74  -3.91    -6.488330    1      
iter:   3  08:19:46  -5.58  -4.13    -6.488330    1      
iter:   4  08:19:46  -6.71  -4.88    -6.488330    1      
iter:   5  08:19:46  -6.93  -4.80    -6.488330    1      
iter:   6  08:19:46  -7.25  -5.20    -6.488330    1      
iter:   7  08:19:46  -8.12  -5.45    -6.488330    1      

Converged after 7 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -21715.239799)

Kinetic:         +6.648859
Potential:       -7.520203
External:        +0.000000
XC:              -5.579278
Entropy (-ST):   -0.277259
Local:           +0.100921
--------------------------
Free energy:     -6.626959
Extrapolated:    -6.488330

 Band  Eigenvalues  Occupancy
    0    -20.67269    2.00000
    1    -15.28852    2.00000
    2     -9.72238    2.00000
    3     -8.76408    2.00000
    4     -8.76408    2.00000
    5     -5.30820    1.00000
    6     -5.30820    1.00000
    7     -1.24534    0.00000
    8      0.40156    0.00000
    9      1.92046    0.00000
   10      1.92046    0.00000
   11      1.97100    0.00000
   12      2.00253    0.00000
   13      2.01934    0.00000
   14      2.33609    0.00000
   15      3.12979    0.00000

Fermi level: -5.30820

Gap: 0.000 eV
Transition (v -> c):
  (s=0, k=0, n=5, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=6, [0.00, 0.00, 0.00])

Forces in eV/Ang:
  0 S     0.03445   -0.00000    0.00000
  1 S    -0.03445   -0.00000    0.00000

FIRE:    4 08:19:46       -6.626959*       0.0344
PBE -6.488329962098764
TPSS -16.651796108578356
M06-L -14.557504096492588
Timing:                                      incl.     excl.
-------------------------------------------------------------------
Density initialized from wave functions:     0.301     0.014   0.1% |
 Symmetrize density:                         0.288     0.288   2.2% ||
Forces:                                      0.034     0.034   0.3% |
Hamiltonian:                                 0.432     0.000   0.0% |
 Atomic:                                     0.001     0.001   0.0% |
  XC Correction:                             0.000     0.000   0.0% |
 Calculate atomic Hamiltonians:              0.001     0.001   0.0% |
 Communicate:                                0.173     0.173   1.3% ||
 Hartree integrate/restrict:                 0.006     0.006   0.0% |
 Initialize Hamiltonian:                     0.000     0.000   0.0% |
 Poisson:                                    0.138     0.002   0.0% |
  Communicate from 1D:                       0.017     0.017   0.1% |
  Communicate from 2D:                       0.032     0.032   0.2% |
  Communicate to 1D:                         0.030     0.030   0.2% |
  Communicate to 2D:                         0.017     0.017   0.1% |
  FFT 1D:                                    0.017     0.017   0.1% |
  FFT 2D:                                    0.021     0.021   0.2% |
 XC 3D grid:                                 0.110     0.110   0.8% |
 vbar:                                       0.002     0.002   0.0% |
LCAO initialization:                         0.038     0.021   0.2% |
 LCAO eigensolver:                           0.002     0.000   0.0% |
  Calculate projections:                     0.000     0.000   0.0% |
  DenseAtomicCorrection:                     0.000     0.000   0.0% |
  Distribute overlap matrix:                 0.001     0.001   0.0% |
  Orbital Layouts:                           0.000     0.000   0.0% |
  Potential matrix:                          0.000     0.000   0.0% |
 LCAO to grid:                               0.001     0.001   0.0% |
 Set positions (LCAO WFS):                   0.014     0.011   0.1% |
  Basic WFS set positions:                   0.001     0.001   0.0% |
  Basis functions set positions:             0.000     0.000   0.0% |
  P tci:                                     0.000     0.000   0.0% |
  ST tci:                                    0.001     0.001   0.0% |
  mktci:                                     0.001     0.001   0.0% |
Redistribute:                                0.001     0.001   0.0% |
SCF-cycle:                                  10.938     0.040   0.3% |
 Davidson:                                   2.914     1.154   8.7% |--|
  Apply hamiltonian:                         0.305     0.305   2.3% ||
  Subspace diag:                             0.437     0.004   0.0% |
   calc_h_matrix:                            0.343     0.032   0.2% |
    Apply hamiltonian:                       0.310     0.310   2.3% ||
   diagonalize:                              0.044     0.044   0.3% |
   rotate_psi:                               0.047     0.047   0.3% |
  calc. matrices:                            0.894     0.299   2.2% ||
   Apply hamiltonian:                        0.595     0.595   4.5% |-|
  diagonalize:                               0.035     0.035   0.3% |
  rotate_psi:                                0.088     0.088   0.7% |
 Density:                                    3.573     0.001   0.0% |
  Atomic density matrices:                   0.033     0.033   0.2% |
  Mix:                                       0.138     0.138   1.0% |
  Multipole moments:                         0.005     0.005   0.0% |
  Pseudo density:                            3.396     0.012   0.1% |
   Symmetrize density:                       3.384     3.384  25.4% |---------|
 Hamiltonian:                                4.398     0.003   0.0% |
  Atomic:                                    0.010     0.010   0.1% |
   XC Correction:                            0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:             0.014     0.014   0.1% |
  Communicate:                               1.730     1.730  13.0% |----|
  Hartree integrate/restrict:                0.063     0.063   0.5% |
  Poisson:                                   1.429     0.024   0.2% |
   Communicate from 1D:                      0.202     0.202   1.5% ||
   Communicate from 2D:                      0.342     0.342   2.6% ||
   Communicate to 1D:                        0.291     0.291   2.2% ||
   Communicate to 2D:                        0.208     0.208   1.6% ||
   FFT 1D:                                   0.160     0.160   1.2% |
   FFT 2D:                                   0.202     0.202   1.5% ||
  XC 3D grid:                                1.129     1.129   8.5% |--|
  vbar:                                      0.020     0.020   0.1% |
 Orthonormalize:                             0.013     0.000   0.0% |
  calc_s_matrix:                             0.002     0.002   0.0% |
  inverse-cholesky:                          0.004     0.004   0.0% |
  projections:                               0.005     0.005   0.0% |
  rotate_psi_s:                              0.003     0.003   0.0% |
Set symmetry:                                0.004     0.004   0.0% |
Other:                                       1.558     1.558  11.7% |----|
-------------------------------------------------------------------
Total:                                                13.306 100.0%

Memory usage: 98.13 MiB
Date: Fri Mar 19 08:19:48 2021
