from gpaw import GPAW

for ia in range(2, 4):
    c = GPAW('PBE_h0.2_{0}_F1s.gpw'.format(ia), txt=None)
    c.converge_wave_functions()
    hamiltonian = c.hamiltonian
    density = c.density
    EtH = hamiltonian.finegd.integrate(hamiltonian.vHt_g,
                                       density.nt_g)
    print('********', ia, EtH)
