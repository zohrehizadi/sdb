from sdb.molecules import find


def test_name():
    mol = find(composition='OH2')
    assert mol.name == 'H2O'
