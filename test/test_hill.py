from sdb.molecules import hill


def test_hill():
    assert hill('OCHCCOO4') == 'C3HO6'
    assert hill('SnF2Cl2') == 'Cl2F2Sn'
    assert hill('AsSnH8F2Cl2C6') == 'C6H8AsCl2F2Sn'
