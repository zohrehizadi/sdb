from ase import Atoms
from sdb.experiment import Molecule as Experiment
from sdb.molecules import Molecule


def test_F4S_exp():
    exp = Experiment('F4S')

    print('--- S(2p3/2)')
    print(exp.xps_by_element('S', '2p3/2'))
    assert len(exp.xps_by_element('S', '2p3/2')) == 1
    print(exp.xps_by_element(vstate='S(2p3/2)', references=True))
    assert len(exp.xps_by_element('S', '2p3/2', references=True)) == 2

    print('--- F(1s)')
    try:
        print(exp.xps_by_element(vstate='F(1s)'))
    except TypeError:
        pass  # this should fail due to different chemical environments
    else:
        assert 0

    for ia in [1, 2, 3, 4]:
        print(exp.xps_by_index(ia, vstate='F(1s)'))
    # the same chemical environment
    assert (exp.xps_by_index(1, vstate='F(1s)')
            == exp.xps_by_index(2, vstate='F(1s)'))
    # different chemical environment
    assert (exp.xps_by_index(1, vstate='F(1s)')
            > exp.xps_by_index(3, vstate='F(1s)'))
    # the same chemical environment
    assert (exp.xps_by_index(3, vstate='F(1s)')
            == exp.xps_by_index(4, vstate='F(1s)'))


def test_F4S_calc():
    mol = Molecule('F4S')
    assert isinstance(mol.atoms, Atoms)
    assert len(mol.atoms) == 5

    xps = mol.xps('F')
    assert len(xps) == 4


def test_H2O():
    exp = Experiment('H2O')
    print(exp)
