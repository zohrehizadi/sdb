from sdb.utilities.latex import decorate_formula


def test_decorate():
    assert decorate_formula('CH3(CH2)42SH') == 'CH$_3$(CH$_2$)$_{42}$SH'
