import numpy as np
import pytest

from sdb.molecules import find, find_element


@pytest.mark.xfail  # XXX
def test_from_name():
    mol = find(name='water')
    print(mol)


def test_from_composition():
    mol = find(composition='OH2')
    assert len(mol.atoms) == 3

    try:
        find(composition='O2H2')
    except RuntimeError:
        pass


def test_from_file(test_data):
    mol = find(file=test_data / 'molecules' / 'H2O' / 'GPAW_PBE' / 'ini.xyz')
    assert len(mol.atoms) == 3


@pytest.mark.xfail  # XXX
def test_find_element():
    assert len(find_element('O')) == 1


def test_find_emt():
    molecule = find(composition='OH2', code='EMT')
    assert molecule.relaxed()


def test_find_isomers():
    mols = find('F2LiO2P')
    assert len(mols) == 2

    mol = find(name='F2LiO2P/symmetric')
    assert hasattr(mol, 'atoms')


def test_find_spin_projection():
    mol = find('CH2')
    assert mol.atoms.get_initial_magnetic_moments().sum() \
           == pytest.approx(0)

    def calc_Sz(calc):
        print('kpts', calc.kpts)
        if calc.get_number_of_spins() == 1:
            return 0
        else:
            return np.sum(calc.kpts[0].f_n - calc.kpts[1].f_n)

    for Sz in [0, 2]:
        mol = find('CH2', Sz=Sz)
        assert calc_Sz(mol.atoms.calc) == pytest.approx(Sz)
