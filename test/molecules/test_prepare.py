from ase import io
from ase.build import molecule

from sdb.molecules import prepare


def test_fresh(tmpdir):
    """Prepare a noexisting hill"""

    atoms = molecule('cyclobutene')

    basedir = tmpdir / 'molecules'
    prepare(atoms, basedir=basedir)
    directory = basedir / 'C4H6' / 'GPAW_PBE'
    assert (directory / 'relax.py').exists()
    assert io.read(str(directory / 'ini.xyz')) == atoms
