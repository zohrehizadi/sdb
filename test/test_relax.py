import pytest
from pathlib import Path

from ase.build import molecule
from ase.calculators.emt import EMT
from ase.vibrations import Vibrations

from sdb.molecules import Molecule


def initialize(atoms):
    atoms.calc = EMT()
    return atoms


@pytest.fixture
def ch4(tmp_path):
    """Create a nonexisting Molecule from an atoms object"""
    name = 'CH4'
    atoms = molecule(name)
    code = 'EMT'

    mol = Molecule.create(name, path=tmp_path, code=code)
    assert isinstance(mol, Molecule)

    mol.atoms = atoms
    mol.relax(initialize=initialize)
    return mol


def test_ch4(ch4):
    """Create a nonexisting Molecule from an atoms object"""
    assert ch4.relaxed()
    assert (Path(ch4.directory) / 'relaxed.traj').exists()


def test_ch4_vibrations(ch4, tmp_path):
    vib = ch4.vibrations(initialize=initialize)
    assert isinstance(vib, Vibrations)
    assert (ch4.directory / 'vib_relaxed.traj').is_dir()
