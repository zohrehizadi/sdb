import pytest

from sdb.rates import Eyring_rate
from sdb.thermochemistry import default_T_K


def test_Eyring():
    T_K = default_T_K
    assert Eyring_rate(0, 2 * T_K) == pytest.approx(2 * Eyring_rate(0))
