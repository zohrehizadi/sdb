from sdb.molecules import walk


def test_len():
    assert len(walk()) >= 2
    assert len(walk('O')) >= 1
