import yaml
from sdb.molecules import find
from sdb.molecules.pubchem import update


def test_pubchem(test_data):
    mol = find('F4S')

    defname = 'new_definition.yml'
    assert update(mol, defname=defname)

    assert 'pubchemcid' in mol.definition
    with open(mol.base_directory / defname) as f:
        definition = yaml.load(f, Loader=yaml.SafeLoader)

    assert 'pubchemcid' in definition
    (mol.base_directory / defname).unlink()
