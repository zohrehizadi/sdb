from sdb.experiment import Molecule as Experiment
from sdb.xray import xray_experiment


def test_xas_S2():
    exp = Experiment('S2')
    values, refs = xray_experiment('xas', exp, 'S(1s)')
    assert len(values) == len(refs)
    assert len(values) == 2

    exp = Experiment('F4S')
    values, refs = xray_experiment('xas', exp, 'S(1s)')
    assert len(values) == len(refs)
    assert len(values) == 0


def test_xes_H2O():
    exp = Experiment('H2O')
    values, refs = xray_experiment('xes', exp, 'O(1s)')
    assert len(values)
