from sdb import xps


def test_Si():
    Si2p32 = xps.get_empirical_shift('Si', '2p3/2', so=True)
    Si2p = xps.get_empirical_shift('Si', '2p', so=True)
    Si2p12 = xps.get_empirical_shift('Si', '2p1/2', so=True)
    # shift of 2p1/2 < 2p < 2p3/2
    assert Si2p12 - Si2p < 0
    assert Si2p - Si2p32 < 0
