from pathlib import Path

from sdb.molecules import find
from sdb.xps import state
from sdb.xps.gpaw import setups, calculate_and_write


def test_S2():
    atoms = find('S2').atoms

    nS = len(state['S'])
    setup_list = list(setups(atoms))

    assert len(setup_list) == nS * len(atoms)


class FakeObj():
    pass


class FakeGPAW():
    def __init__(self):
        self.energy = 0
        self.wfs = FakeObj()
        self.wfs.setups = FakeObj()
        self.wfs.setups.Eref = 0

    def set(self, charge, setups):
        energy = {'1s1ch': 100, '2s1ch': 10, '2p1ch': 1}
        self.energy = energy[list(setups.values())[0]] + charge

    def get_potential_energy(self, atoms):
        return self.energy

    def get_xc_difference(self, xc):
        return 0


def test_write_files():
    atoms = find('S2').atoms

    datfname = 'xps.dat'
    atoms.calc = FakeGPAW()

    calculate_and_write(atoms, datfname, charge=-5,
                        nonselfconsistent_xc=FakeObj())
    assert Path(datfname).exists()
