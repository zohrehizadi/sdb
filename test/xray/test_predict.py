import pytest

from sdb.xps.predict import intensities
from sdb.xps import so_splitting


def test_C1s():
    energies = [284, 290]
    ene, intens = intensities(energies, 'C(1s)')
    assert len(ene) == len(energies)
    assert intens.sum() == pytest.approx(len(energies))
    for e0, e1 in zip(energies, ene):
        assert e1 < e0  # shifts is negativ


def test_S2p():
    ene, intens = intensities([162], 'S(2p)')
    assert len(ene) == 2
    assert len(ene) == len(intens)
    assert intens.sum() == 1

    # 2p1/2 intensity is half of 2p3/2, but at higher energy
    assert 2 * intens[0] == pytest.approx(intens[1])
    assert ene[0] - ene[1] == pytest.approx(so_splitting['S']['2p'])
