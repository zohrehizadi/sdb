import pytest

from sdb.xps.collect import collect, fit


def test_O():
    entries = collect('O(1s)')

    assert 'H2O' == entries[0].name

    for entry in entries:
        assert len(entry.Eexp.values) == len(entry.references)


def test_F():
    entries = collect('F(1s)')
    assert len(entries) == 2
    assert entries[0].Eexp != entries[1].Eexp

    for entry in entries:
        assert len(entry.Eexp.values) == len(entry.references)

    shift, error = fit(entries)
    assert shift == pytest.approx(8.388820896805738)
