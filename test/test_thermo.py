import pytest
from pathlib import Path

from ase import Atoms
from ase.build import molecule
from ase.calculators.emt import EMT
from ase.optimize import FIRE

from sdb.molecules import Molecule
from sdb.molecules import find
from sdb.thermochemistry import molecule_thermo, thermochemistry, geometry


def test_H2O():
    molecule = find(composition='OH2', code='EMT')

    # previous calculation
    expected = {
        None: 1.8791735093002786,
        'zpe': 2.236755604630236,
        'enthalpy': 2.3533654566572837,
        # 'gibbs': 1.718093936005774  # XXX problems with Gibbs values
    }

    for correction in expected.keys():
        assert expected[correction] == pytest.approx(
            molecule_thermo(molecule, correction))


def test_interface():
    molecule = find(composition='OH2', code='EMT')

    correction = 'gibbs'
    assert (molecule_thermo(molecule, correction)
            == thermochemistry(
                molecule.atoms, molecule.vibrations(), correction))


def test_geometry():
    assert geometry(Atoms('H')) == 'monatomic'

    # XXX do we have a more sensible value for a tiny deviation
    # of linearity?
    for delta, geo in zip([1e-5, 0.2], ['linear', 'nonlinear']):
        atoms = Atoms('H3',
                      positions=[[0, 0, -1], [0, delta, 0], [0, 0, 1]])
        assert geometry(atoms) == geo


def test_zero_vibration():
    """Numerically we my have zero real vibrations as here for flat C2H4"""
    name = 'C2H4'
    code = 'EMT'
    atoms = molecule(name)
    atoms.calc = EMT()

    opt = FIRE(atoms)
    opt.run(fmax=0.05)

    direc = Path('molecules') / name / code
    direc.mkdir(parents=True)
    atoms.write(direc / 'relaxed.traj')

    def initialize(atoms):
        atoms.calc = EMT()
        return atoms

    mol = Molecule(name, 'molecules', code=code)
    mol.vibrations(initialize)

    # XXX problems with Gibbs values
    # assert molecule_thermo(mol, 'gibbs') == pytest.approx(0.7134395923086339)
