import json
from sdb.search.build import update


def test_water(test_data, tmp_path):
    # build tables of names
    update(rootdir=test_data, tabledir=tmp_path)

    with open(tmp_path / 'names.json') as f:
        names = json.loads(f.read())
    # ensure lower case water is there
    assert 'water' in names
    # ensure no lower case h2O or h2o is added
    assert 'h2O' not in names
    assert 'h2o' not in names
