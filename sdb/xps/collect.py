import numpy as np
from scipy.optimize import curve_fit
from typing import List

from sdb.data import EnergyRange, ExpCalc
from sdb.molecules import walk, Molecule
from sdb.experiment import Molecule as Experiment
from sdb.xps import element_state
from sdb.xps import state_Jolly1984


def state_exp(element, state):
    sd = state_Jolly1984[element]
    return '{0}{1}'.format(state, sd[state])


def collect(vstate: str, xc: str = 'PBE', h: float = 0.2,
            box=4, spinpol=False,
            verbose=2) -> List[ExpCalc]:
    """
    element: string, e.g. 'Si'
    state: None or string, e.g. '2p'
      None means default state for the element from xps_state
    xc: string, default 'PBE'
    h: grid spacing
    box: grid spacing around each atom
    spinpol: spin polarized or not
    """
    element, state = element_state(vstate)

    entries = []

    for name in walk(element):
        try:
            exp = Experiment(name)
        except IOError:
            if verbose > 1:
                print(f'{name}: No experimental data')
            continue

        try:
            types = False
            eexp, refs = exp.xps_by_element(
                element, state_exp(element, state), references=True)
            if not len(eexp):
                if verbose > 1:
                    print('No experimental data for', name,
                          'and state {0}({1})'.format(
                              element, state_exp(element, state)))
                continue
        except TypeError:
            types = True
        try:
            calc = Molecule(name)
        except RuntimeError as e:
            print('############### error', name)
            print(e)
            continue

        try:
            ecalc = calc.xps(element, state, xc=xc,
                             h=h, box=box, spinpol=spinpol)
        except (IOError, ValueError):
            if verbose > 1:
                print('No calculated XPS data for', name)
            continue

        if not types:
            if verbose:
                print(f'{name}: adding')
            entries.append(ExpCalc(EnergyRange(eexp),
                                   EnergyRange(ecalc[0][1]),
                                   name, refs))
        else:
            # we have different chemical environments
            haveit = []
            for i, a in enumerate(calc.atoms):
                if a.symbol == element:
                    try:
                        eexp, refs = exp.xps_by_index(
                            i, element, state, references=True)
                    except ValueError as e:
                        if verbose:
                            print('###', e)
                        continue
                    if eexp[0] not in haveit:  # do not double count
                        Eexp = EnergyRange(eexp)
                        for entry in ecalc:
                            if i == entry[0]:
                                Ecalc = EnergyRange(entry[1])
                        if verbose:
                            print(f'{name}: adding')
                        entries.append(ExpCalc(Eexp, Ecalc, name, refs))
                        haveit.append(eexp[0])

    return entries


def fit(exp_calc_list):
    """Fit XPS/XAS shift"""
    # XXX consider errors
    eexp = [e.Eexp.mmp()[0] for e in exp_calc_list]
    ecalc = [e.Ecalc.mmp()[0] for e in exp_calc_list]

    def func(x, de):
        return x - de
    popt, pcov = curve_fit(func, eexp, ecalc)

    # return in convention of DOI 10.1103/PhysRevB.94.041112
    return -popt[0], np.sqrt(np.diag(pcov))[0]
