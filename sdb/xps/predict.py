import numpy as np

from . import _empirical_shift
from . import element_state, get_empirical_shift, so_weight, so_splitting


def intensities(energies_in, vstate: str):
    element, state = element_state(vstate)

    # find so splitting
    if 's' in state:
        so_split = 0
    else:
        so_split = so_splitting[element][state]

    # find shift and correct for spin-orbit splitting
    shift_dict = _empirical_shift[element]
    for so, w in so_weight[state].items():
        key = state + so
        if key in shift_dict:
            shift = - get_empirical_shift(element, key)
            shift -= w * so_split

    # reconstruct all so contributions
    weights = []
    shifts = []
    for so, w in so_weight[state].items():
        wabs = abs(w)
        if wabs:
            weights.append(wabs)
            shifts.append(shift + w * so_split)

    energies = []
    intensities = []

    for energy in energies_in:
        for shift, intens in zip(shifts, weights):
            energies.append(energy + shift)
            intensities.append(intens)

    return np.array(energies), np.array(intensities)
