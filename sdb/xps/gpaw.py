from typing import Iterator
from ase import Atoms
from ase.parallel import paropen
from ase.units import Hartree

from . import state


def setups(atoms: Atoms) -> Iterator:
    """iterator looping over all atoms and respective core-state names"""
    for ia, atom in enumerate(atoms):
        state_list = state.get(atom.symbol, [])
        for corestate in state_list:
            yield ia, corestate


def calculate_and_write(atoms: Atoms,
                        datfname: str,
                        charge: float = 0, *,
                        nonselfconsistent_xc=None) -> None:
    """Calculate all core-holes and write the corresponding file"""
    E0 = atoms.get_potential_energy()
    E0 += atoms.calc.wfs.setups.Eref * Hartree
    if nonselfconsistent_xc is not None:
        E0 += atoms.calc.get_xc_difference(nonselfconsistent_xc)

    with paropen(datfname, 'a') as f:
        print('# energy', E0, file=f)
        print('# i  E(1s1ch)  Eref  # element', file=f)
        for ia, corestate in setups(atoms):
            atoms.calc.set(charge=charge,
                           setups={ia: f'{corestate}1ch'})
            E = atoms.get_potential_energy()
            E += atoms.calc.wfs.setups.Eref * Hartree
            if nonselfconsistent_xc is not None:
                E += atoms.calc.get_xc_difference(nonselfconsistent_xc)

            print(ia, E - E0, E, f'# {atoms[ia].symbol}({corestate})', file=f)
            f.flush()
