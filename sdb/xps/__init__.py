import os
import numpy as np

from glob import glob
from ase.io.gpaw_out import read_gpaw_out

data = {}

# states to consider
# first entry is used as "default state"
state = {
    "B": ["1s"],
    "C": ["1s"],
    "N": ["1s"],
    "O": ["1s"],
    "F": ["1s"],
    "Al": ["2p"],
    "Si": ["2p"],
    "P": ["2p", "2s", "1s"],
    "S": ["2p", "2s", "1s"],
    "Cl": ["2p"],
    "Ti": ["2p"],
    "Fe": ["2p"],
    "Ni": ["2p"],
    "Ge": ["3p"],
    "Se": ["3d"],
    "Mo": ["3d"],
    "Br": ["3d"],
    "Sn": ["3d"],
    "Te": ["4d"],
    "I": ["3d"],
    "Pt": ["2p", "4f"],
}

# State and spin projection given in
# Jolly, William L.; Bomben, K. D. and Eyermann, C. J.
# At. Data Nucl. Data Tables 31 (1984) 433-493
state_Jolly1984 = {
    "B": {"1s": ""},
    "C": {"1s": ""},
    "N": {"1s": ""},
    "O": {"1s": ""},
    "F": {"1s": ""},
    "Al": {"2p": "3/2"},
    "Si": {"2p": ""},
    "P": {"1s": "", "2s": "", "2p": "3/2"},
    "S": {"1s": "", "2s": "", "2p": "3/2"},
    "Cl": {"2p": "3/2"},
    "Ti": {"2p": "3/2"},
    "Fe": {"2p": "3/2"},
    "Ni": {"2p": "3/2"},
    "Ge": {"3p": "3/2"},
    "Se": {"3d": ""},
    "Mo": {"3d": "5/2"},
    "Br": {"3d": "5/2"},
    "Sn": {"3d": "5/2"},
    "Te": {"4d": ""},
    "I": {"3d": "5/2"},
}

# experimental spin-orbit splitting
so_splitting = {
    "Al": {
        "2p": 0.44,  # Agui; Phys. Rev. B 59 (1999) 10792
    },
    "S": {
        # Castner; Langmuir 12 (1996) 5083-5086 doi: 10.1021/la960465w
        "2p": 1.2,
    },
    "Si": {
        "2p": 0.6,  # Takagaki; Appl. Surf. Sci. 92 (1996) 287
    },
    "Sn": {
        # 3d3/2 vs 3d5/2 Choi W.-K., Jung H.-J., Koh S.-K.;
        # J. Vac. Sci. Technol. A 14, 359 (1996)
        "3d": 8.42,
    },
    "Ti": {
        "2p": 6.4,  # 10.1039/D1FD00103E
    },
    "V": {
        "2p": 7.7,  # 10.1039/D1FD00103E
    },
    "Mn": {
        "2p": 9.7,  # 10.1039/D1FD00103E
    },
    "Fe": {
        "2p": 11.2,  # 10.1039/D1FD00103E
    },
    "Co": {
        "2p": 13.1,  # 10.1039/D1FD00103E
    },
}
so_weight = {
    "1s": {
        "": 1,
    },
    # Energy of 2p1/2 > energy  of 2p3/2
    "2p": {
        "": 0,
        "1/2": 2.0 / 6.0,
        "3/2": -4.0 / 6.0,
    },
    "3d": {
        "": 0,
        "1/2": 2 / 12,  # weight 2/12
        "3/2": 4 / 12,  # weight 4/12
        "5/2": 6 / 12,  # weight 6/12
    },
}

# Empirical shifts depending on element, state and xc
_empirical_shift = {
    "C": {
        "1s": {
            "PBE": {0.2: 6.07, 0.18: 6.08, 0.16: 6.11, 0.15: 6.10, 0.14: 6.10},
        },
    },
    "N": {
        "1s": {
            "PBE": {0.2: 6.95, 0.18: 7.04, 0.16: 7.11, 0.15: 7.11, 0.14: 7.12},
        },
    },
    "O": {
        "1s": {
            "PBE": {0.2: 7.66, 0.18: 7.77, 0.16: 7.83, 0.15: 7.86, 0.14: 7.88},
        },
    },
    "F": {
        "1s": {
            "PBE": {0.2: 8.44, 0.18: 8.50, 0.16: 8.54, 0.15: 8.55, 0.14: 8.56},
        },
    },
    "Al": {
        "2p3/2": {
            # change due to cluster data DOI:10.1039/C8CP07169A
            "PBE": {0.2: 0.85, 0.14: 0.85},
        },
    },
    "Si": {
        "2p": {
            # change due to cluster data DOI:10.1039/C8CP07169A
            "PBE": {0.2: 0.84, 0.14: 0.84},
        },
    },
    "P": {
        "2p3/2": {
            "PBE": {0.2: 0.67, 0.16: 0.67, 0.15: 0.66, 0.14: 0.66},
        },
    },
    "S": {
        "2p3/2": {
            "PBE": {0.2: 0.91, 0.18: 0.88, 0.16: 0.87, 0.15: 0.87, 0.14: 0.86},
        },
        "1s": {
            # 10.1039/D3CP02285D
            "PBE": {0.2: 12.97},
        },
    },
    "Ti": {
        "2p3/2": {
            "PBE": {0.2: 0.61686792, 0.15: 0.6200138},
        },
    },
    "Sn": {
        "3d5/2": {
            "PBE": {0.2: -0.2868833, 0.18: -0.30739991, 0.15: -0.32405464},
        },
    },
}


def element_state(vstate):
    """Split element and state given as 'C(1s)' or as 'C'"""
    w = vstate.replace("(", " ")
    w = w.replace(")", "")
    ws = w.split()
    try:
        elem, st = ws
    except ValueError:
        elem = ws[0]
        st = state[elem][0]  # default state
    return elem, st


def shift(E, element, state, xc="PBE", h=0.2):
    delta = get_empirical_shift(element, state, xc="PBE", h=0.2)
    return E - delta


def get_empirical_shift(element, state, xc="PBE", h=0.2,
                        so=False, extrapolate=False):
    """Return empirical shift in [eV].

    so: correct for experimental spin-orbit splitting
    extrapolate: extrapolate out of tabulated h values
    """
    err = "No data for element {0}".format(element)
    try:
        data = _empirical_shift[element]
        haveit = False
        try:
            data = data[state]
            e_so = 0
            haveit = True
        except KeyError:
            if so:
                # we do not have it explicitely, but maybe for another spin
                # projection
                st, spin = state[:2], state[2:]

                def get_so(element, st, spin):
                    # get spin_orbit shift relative to mean
                    return so_weight[st][spin] * so_splitting[element][st]

                e_so = -get_so(element, st, spin)

                for spin in so_weight[st]:
                    stspin = st + spin
                    if stspin in data:
                        data = data[stspin]
                        e_so += get_so(element, st, spin)
                        haveit = True

        if not haveit:
            raise RuntimeError(err + " and state {0}.".format(state))

        try:
            data = data[xc]

            # interpolate
            hl = np.array(sorted(data.keys()))
            if (h > hl.max() or h < hl.min()) and not extrapolate:
                raise RuntimeError(
                    "grid spacing out of "
                    + "[{0}:{1}].".format(hl.min(), hl.max())
                )
            sl = [data[x] for x in hl]
            return e_so + np.interp(h, hl, sl)

        except KeyError:
            raise RuntimeError(err + ", state {0} and {1}.".format(state, xc))
    except KeyError:
        raise RuntimeError(err + ".")


class XPSMolecule:
    def __init__(self, directory):
        tlst = glob(os.path.join(directory, "relax.py.out*"))
        if not len(tlst):
            raise ValueError
        elif len(tlst) > 1:
            raise RuntimeError("more than one structure in " + directory)

        self.directory = directory
        #        self.atoms = io.read(tlst[0])
        self.atoms = read_gpaw_out(tlst[0])
        self.fmax = np.sqrt(self.atoms.get_forces() ** 2).max()

    def name(self):
        return os.path.split(self.directory)[-1]

    def datadir(self):
        """Return data directory with full path"""
        dirlst = glob(os.path.join(self.directory, "*1ch"))
        assert len(dirlst) < 2
        try:
            return dirlst[0]
        except IndexError:
            return os.path.join(self.directory, "1ch")

    def datafile(self, xc="PBE", h=0.2, box=4):
        """New style data file name"""
        return os.path.join(
            self.datadir(), "1ch_by_atom_{0}_h{1}_box{2}.dat".format(
                xc, h, box)
        )

    def xps(self, element, state=None, xc="PBE", h=0.2, box=4):
        datadir = self.datadir()
        if not os.path.exists(datadir):
            raise ValueError

        data = []

        if xc == "PBE":
            try:
                # try to use old data
                fname = os.path.join(datadir, "CN_1s1ch_by_atom.dat")
                for line in open(fname).readlines():
                    try:
                        word = line.split()
                        i = int(word[0])
                        Ech = float(word[1])
                        elem = word[3]
                        assert element == elem
                        try:
                            dh = float(word[4])
                            assert h == dh
                            dbox = float(word[5])
                            assert box == dbox
                        except IndexError:
                            # very old data
                            assert h == 0.2 and box == 4
                        data.append((i, Ech))
                    except (ValueError, AssertionError):
                        pass
                if len(data):
                    return data
            except IOError:
                pass

        try:
            fname = os.path.join(
                datadir,
                "CNO1s1ch_by_atom_{0}_h{1}_box{2}.dat".format(xc, h, box)
            )
            for line in open(fname).readlines():
                try:
                    word = line.split()
                    i = int(word[0])
                    Ech = float(word[1])
                    elem = word[3].split("(")[0]
                    assert element == elem
                    data.append((i, Ech))
                except (ValueError, AssertionError):
                    pass
            if len(data):
                return data
        except IOError:
            pass

        fname = self.datafile(xc, h, box)
        for line in open(fname).readlines():
            try:
                word = line.split()
                i = int(word[0])
                Ech = float(word[1])
                elem = word[3]
                assert element == elem
                data.append((i, Ech))
            except (ValueError, AssertionError):
                pass
        if len(data):
            return data

        raise ValueError
