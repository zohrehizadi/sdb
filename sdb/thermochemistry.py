from typing import Optional

from ase.units import kB
from ase.thermochemistry import IdealGasThermo
from ase import Atoms
from ase.vibrations import Vibrations

default_T_K = 298.15  # 25°C
default_pressure = 101325.


def geometry(atoms: Atoms, tiny: float = 1e-8) -> str:
    """Determine geometry in ase style"""
    if len(atoms) == 1:
        return 'monatomic'

    inertia_c = atoms.get_moments_of_inertia()
    if (inertia_c < tiny).any():
        return 'linear'

    return 'nonlinear'


def molecule_thermo(molecule, correction: Optional[str],
                    T_K: float = default_T_K,
                    pressure: float = default_pressure,
                    tiny: float = 1e-12,) -> float:
    return thermochemistry(
        molecule.atoms, molecule.vibrations(), correction,
        T_K, pressure, tiny)


def thermochemistry(atoms: Atoms, vibrations: Optional[Vibrations],
                    correction: Optional[str] = 'zpe',
                    T_K: float = default_T_K,
                    pressure: float = default_pressure,
                    tiny: float = 1e-12,) -> float:
    assert correction in [None, 'zpe', 'enthalpy', 'gibbs']

    E0 = atoms.get_potential_energy()
    if correction is None:
        return E0

    magmom = atoms.calc.results.get(
        'magmom', atoms.get_initial_magnetic_moments().sum())

    geo = geometry(atoms)
    if geo == 'monatomic':
        vibene_v = []
    else:
        vibene_v = vibrations.get_energies().real
        vibene_v[vibene_v == 0] = tiny  # avoid 0 / 0

    igth = IdealGasThermo(vibene_v, geo, atoms=atoms,
                          symmetrynumber=1, spin=magmom / 2)

    if correction == 'zpe':
        return E0 + igth.get_ZPE_correction()
    if correction == 'enthalpy':
        return E0 + igth.get_enthalpy(T_K, verbose=False)

    # gibbs
    E0 += igth.get_enthalpy(T_K, verbose=False)
    # we suppress zero vibrations for entropy
    vibene_v = vibrations.get_energies().real
    vibene_v[vibene_v == 0] = 100 * kB * T_K  # will not contribute
    igth = IdealGasThermo(vibene_v, geo, atoms=atoms,
                          symmetrynumber=1, spin=magmom / 2)

    return E0 - T_K * igth.get_entropy(T_K, pressure, verbose=False)
