import os
from pathlib import Path

rootdir = Path(os.environ.get('STRUCTURE_DATABASE_DIR'))
if rootdir is None:
    raise RuntimeError('No data, please set STRUCTURE_DATABASE_DIR')
