import numpy as np

from ase.units import kB, J, _hplanck

from .thermochemistry import default_T_K


def Arrhenius_rate(energy: float, omega: float,
                   T_K: float = default_T_K) -> float:
    """Arrhenius rate in [1/s]

    energy: activation energy [eV]
    omega: pre-exponential factor in [1/s]
    T: Temperature in K
    """
    beta = 1 / kB / T_K
    return np.exp(- beta * energy) * omega


def Eyring_rate(energy: float,
                T_K: float = default_T_K) -> float:
    """Return the Eyring rate in [1/s]

    energy: [eV]
    T: Temperature [K]
    """
    beta = 1 / kB / T_K
    h = _hplanck * J  # eV s
    omega = 1 / (beta * h)
    return Arrhenius_rate(energy, omega, T_K)
