import re


def decorate_formula(formula: str) -> str:
    """Decorate numbers in a formula

    e.g.: 'C9H20' -> 'C$_9$H$_{20}$'
    """
    res = ''
    for part in re.split(r'(\d+)', formula):
        if part.isdigit():
            if len(part) > 1:
                res += '$_{' + part + '}$'
            else:
                res += f'$_{part}$'
        else:
            res += part
    return res
