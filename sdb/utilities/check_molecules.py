from __future__ import print_function, division
import os
from ase.atoms import Atoms

from sdb.molecules import walk, Molecule
from sdb.experiment import Molecule as Experiment

v = -1


def warn(obj, indent=2):
    if v > 0:
        print(" " * indent, obj)


def check_experiment():
    # check Experiment
    try:
        Experiment(name)
    except IOError:
        warn("No experimental data for " + name)


def check_molecule():
    # check Molecule
    try:
        Molecule(name)
    except RuntimeError as e:
        warn("error:" + str(e))


if __name__ == "__main__":
    print("Checking names..", end="")
    moved = []
    for name in walk():
        comp = name.split(os.sep)[0]
        hill = Atoms(comp).get_chemical_formula()
        if comp not in moved and comp != hill:
            if not len(moved):
                print("please apply")
            moved.append(comp)
            print("mv", comp, hill)
    if not len(moved):
        print("ok")
