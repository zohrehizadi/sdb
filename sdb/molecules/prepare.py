from pathlib import Path
import shutil
from ase import Atoms

import sdb
from . import basedir, default_code


def prepare(atoms: Atoms, basedir=basedir, code: str = default_code):
    hill = atoms.get_chemical_formula('hill')
    directory = Path(basedir) / hill

    if not directory.exists():
        _add_files(directory, atoms, code)
        _message(directory, code)
    else:
        assert 0, f'{directory} exists'


def _add_files(directory: Path, atoms: Atoms, code: str = default_code):
    subdir = directory / code
    subdir.mkdir(parents=True)
    atoms.write(subdir / 'ini.xyz')

    scripts = Path(sdb.__file__).parent / 'scripts' / code
    fname = 'relax.py'
    shutil.copy(scripts / fname, subdir)


def _message(directory: Path, code: str = default_code):
    subdir = directory / code
    print(f'Relax the structure in {subdir} as the next step.')
