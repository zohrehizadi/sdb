from glob import glob
import re
import os
import yaml
from pathlib import Path
from typing import Callable, Optional

from ase import Atoms, io
from ase.optimize import FIRE
from ase.vibrations import Vibrations

import sdb
from sdb.utilities import lc_dict
from sdb.xps import state as state_xps
from sdb.xps import element_state


# known calculator names
calc_names = ["GPAW", "VASP", "NOTB", "matsci", 'EMT']
# data directory
basedir = os.path.join(sdb.rootdir, "molecules")

default_code = 'GPAW_PBE'


def elements(name):
    """Return hash with elements and number"""
    elems = {}

    def add(element):
        lst = [a for a in re.split(r"([A-Z][a-z]*)", element) if a]
        if lst[0] not in elems:
            elems[lst[0]] = 0
        try:
            elems[lst[0]] += int(lst[1])
        except IndexError:
            elems[lst[0]] += 1

    elems = {}
    for a in re.split(r"([A-Z][a-z]*[\d]*)", name):
        if a:
            add(a)

    return elems


def hill(name):
    """Return Hill notation

    (C, H,  then alphabetic counting of elements).
    see https://libguides.uml.edu/c.php?g=110997&p=719419
    """
    elems = elements(name)

    keys = []
    other = []
    for elem in sorted(elems.keys()):
        if elem == 'C' or elem == 'H':
            keys.append(elem)
        else:
            other.append(elem)
    keys += other

    res = ""
    for a in keys:
        res += a
        if elems[a] > 1:
            res += str(elems[a])
    return res


class Molecule:
    """Molecule with all info."""

    def __init__(self, name, path=None, code=default_code, Sz: float = 0):
        self.hill = hill(name.split(os.sep)[0])
        self.code = code
        self.name = name
        self.Sz = Sz
        if path is None:
            path = basedir

        self.base_directory = Path(path) / name
        if not os.path.exists(self.base_directory):
            raise RuntimeError("No molecule " + name)

        # read definition info
        try:
            data = yaml.load(
                open(os.path.join(
                    self.base_directory, "definition.yml"), "r"),
                Loader=yaml.SafeLoader,
            )
            self.definition = lc_dict(data)
        except IOError:
            pass

        self.directory = Path(self.base_directory) / code
        if not os.path.exists(self.directory):
            raise RuntimeError("No " + code + " info for " + self.hill)

        self._atoms = None

    @property
    def atoms(self):
        if self._atoms is None:
            if self.Sz == 0:
                tnames = ["relax.traj", "gpaw.traj", "relax.py.out*",
                          "relaxed.traj", "job_*.out",]
            else:
                tnames = [f"relax_Sz{self.Sz}.py.out*"]

            for tname in tnames:
                fname = glob(os.path.join(self.directory, tname))
                if len(fname) > 1:
                    raise RuntimeError(
                        "More than one relaxation file found "
                        + "in " + self.directory
                    )
                try:
                    self.filename = fname[0]
                    self._atoms = io.read(fname[0])
                    break
                except (IOError, IndexError, io.formats.UnknownFileTypeError):
                    pass
            if self._atoms is None:
                raise RuntimeError("No " + self.code + " calculation for "
                                   + self.hill)
        return self._atoms

    @atoms.setter
    def atoms(self, value):
        if isinstance(value, Atoms):
            self._atoms = value
        else:
            self.filename = value
            self._atoms = io.read(value)

    def relax(self, fmax: float = 0.05,
              initialize: Optional[Callable] = None,
              relaxed_traj: Optional[str] = 'relaxed.traj',
              optimizer=FIRE) -> None:
        if self.relaxed(fmax):
            return

        if initialize is not None:
            self.atoms = initialize(self.atoms)
        opt = optimizer(self.atoms)
        opt.run(fmax=fmax)

        if relaxed_traj is not None:
            filename = self.directory / relaxed_traj
            with io.Trajectory(filename, 'w') as traj:
                traj.write(self.atoms)
            self.filename = filename

    def relaxed(self, fmax: float = 0.05) -> bool:
        try:
            myfmax2 = (self.atoms.get_forces()**2).sum(axis=1).max()
            return myfmax2 < fmax**2
        except RuntimeError:  # probably no calculator
            return False

    def vibrations(self,
                   initialize: Optional[Callable] = None
                   ) -> Vibrations:
        if initialize is not None:
            self.atoms = initialize(self.atoms)
        else:
            self.atoms  # XXX implictely read, a bit strange

        filename = Path(self.filename)
        name = filename.parent / f'vib_{filename.name}'
        vib = Vibrations(self.atoms, name=name)
        vib.run()

        return vib

    def xps(
        self, element, state=None, xc="PBE", h=0.2, box=4,
        spinpol=False, fname=None
    ):
        if state is None:
            _, state = element_state(element)
        else:
            if state not in state_xps[element]:
                raise RuntimeError(
                    "State {0} not known for element {1}".format(
                        state, element)
                )

        data = []
        spinext = ""
        if spinpol:
            spinext = "_spin"
        if fname is None:
            fname = os.path.join(
                self.directory,
                "1ch",
                "1ch_by_atom_{0}_h{1}_box{2}{3}.dat".format(
                    xc, h, box, spinext),
            )
        else:
            fname = os.path.join(self.directory, "1ch", fname)

        for line in open(fname).readlines():
            try:
                word = line.split()
                i = int(word[0])
                Ech = float(word[1])
                elem, st = element_state(word[3])
                assert element == elem
                assert state == st
                data.append((i, Ech))
            except (ValueError, AssertionError):
                pass
            else:
                assert f'Problems with file {fname}'
        if len(data):
            return data

        raise ValueError

    def __str__(self):
        return " ".join([self.__class__.__name__ + ":",
                         str(self.directory)])

    @classmethod
    def create(cls, name, path=basedir, code=default_code):
        """Create Molecule even if it does not exist"""
        directory = Path(path) / hill(name) / code
        directory.mkdir(parents=True, exist_ok=True)

        return cls(name, path=path, code=code)


def walk(select=None, datadir=basedir):
    """Return the list of all available molecules (including isomers).

    Examples:
    for name in walk():
    ...
    for name in walk('O'):  # select all molecules containing oxygen
    ...
    """

    def select_is_in(name, select):
        if not select:
            return True
        if select in elements(name):
            return True
        return False

    alldirs = [
        d
        for d in os.listdir(datadir)
        if os.path.isdir(os.path.join(datadir, d)) and select_is_in(d, select)
    ]

    calcdirs = []
    for composition in alldirs:
        calcdirs += isomer_directories(composition)
    return calcdirs


def isomer_directories(composition):
    res = []
    for root, dirs, files in os.walk(os.path.join(basedir, composition)):
        calcs = []
        for d in dirs:
            for cn in calc_names:
                if d.startswith(cn):
                    calcs.append(d)
        if len(calcs):
            res.append(root.split(basedir + os.sep)[1])
    return res


def find(composition=None, file=None, name=None, code=default_code,
         Sz: float = 0):
    """Search the database for a specific molecule"""
    s = False
    if composition is not None:
        s = Atoms(composition)
    if file is not None:
        s = io.read(file)
    if s:
        dirs = isomer_directories(s.get_chemical_formula("hill"))
        if not len(dirs):
            raise RuntimeError("No molecule with composition " + composition)
        elif len(dirs) == 1:
            return Molecule(dirs[0], code=code, Sz=Sz)
        else:
            return [Molecule(d, code=code, Sz=Sz) for d in dirs]

    # name
    import json

    with open(os.path.join(sdb.rootdir, "tables", "names.json")) as f:
        names = json.load(f)
    if name in names:
        # return Molecule(names[name].encode('ascii', 'ignore'))
        return Molecule(names[name])
    raise RuntimeError("No molecule with name " + name)


def find_element(element):
    result = []
    # all directories containing the element

    for d0 in glob("molecules/*"):
        level1 = os.path.split(d0)[-1]
        elements = [a for a in re.split(r"([A-Z][a-z]*)", level1) if a]
        if element in elements:
            try:  # level1 molecule
                result.append(Molecule(d0))
            except ValueError:
                # level2 molecules
                for d1 in [
                    d for d in os.listdir(d0)
                        if os.path.isdir(os.path.join(d0, d))
                ]:
                    result.append(Molecule(os.path.join(d0, d1)))
    return result
