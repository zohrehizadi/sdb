from ase.data.pubchem import pubchem_search
import yaml

from . import Molecule


def update(mol: Molecule, defname: str = 'deginition.yml') -> bool:
    """Update Pibchem Data

    Returns True if something was changed
    """
    smiles = mol.definition['smiles']
    res = pubchem_search(smiles=smiles)

    # set and/or check pubchemcid
    pccidkey = 'PUBCHEM_COMPOUND_CID'
    cidkey = 'pubchemcid'

    changed = False
    if pccidkey in res.data:
        if cidkey in mol.definition:
            assert res.data[pccidkey] == mol.definition[cidkey]
        else:
            mol.definition[cidkey] = int(res.data[pccidkey])
            with open(mol.base_directory / defname, 'w') as f:
                changed = True
                f.write(yaml.dump(mol.definition))

    return changed
