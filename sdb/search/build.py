from pathlib import Path
import json
import sys
import yaml
import six

import sdb
from sdb.utilities import lc_dict
from sdb.molecules import walk


def update(select=None,
           rootdir=sdb.rootdir,
           tabledir=sdb.rootdir / 'tables',
           verbose=False,) -> None:
    """Build tables for names and ids"""
    nl = {}
    idl = {}

    # add to name list
    def add2nl(k: str, name: str, lc_also: bool = False):
        if lc_also:
            add2nl(k.lower(), name)
        if k in nl and nl[k] != name:
            try:
                nl[k].append(name)
            except AttributeError:
                nl[k] = [nl[k], name]
        nl[k] = name

    datadir = Path(rootdir) / 'molecules'
    for name in walk(select, datadir):
        add2nl(name, name)
        if verbose:
            sys.stdout.write("\033[K")
            print(name + "\r", end="")
        sys.stdout.flush()

        try:
            data = yaml.load(
                open(datadir / name / 'definition.yml'),
                Loader=yaml.SafeLoader,)
            definition = lc_dict(data)

            for key in ["formula", "latexname", "name", "shortname"]:
                if key in definition:
                    val = definition[key]
                    if not isinstance(val, six.string_types) and hasattr(
                        val, "__iter__"
                    ):
                        # this is a list
                        for item in val:
                            add2nl(item, name, True)
                    else:
                        add2nl(val, name, True)

            # add to id list
            for key in definition:
                if key.lower().endswith("id"):
                    val = definition[key]
                    if val in idl:
                        try:
                            idl[val].append(name)
                        except AttributeError:
                            idl[val] = [idl[val], name]
                    else:
                        idl[val] = name
        except IOError:
            pass

    tabledir = Path(tabledir)
    with open(tabledir / "names.json", "w") as f:
        json.dump(nl, f, indent=4)
    with open(tabledir / "ids.json", "w") as f:
        json.dump(idl, f)
    sys.stdout.write("\033[K")
    print("\rUpdated !")


if __name__ == "__main__":
    update()
